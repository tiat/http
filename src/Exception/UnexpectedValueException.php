<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Http
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Http\Exception;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class UnexpectedValueException extends \UnexpectedValueException implements ExceptionInterface {

}
