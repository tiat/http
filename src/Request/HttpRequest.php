<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Http
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Http\Request;

//
use Tiat\Router\Request\AbstractRequest;
use Tiat\Standard\Config\ConfigInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class HttpRequest extends AbstractRequest {
	
	/**
	 * Constructor method for initializing the class with a given configuration.
	 *
	 * @param    ConfigInterface    $_config    Configuration object to set up the class.
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(ConfigInterface $_config) {
	
	}
}
